# Spring Base: A template spring boot application with all the bells and whistles

## Why does this repo exist?
Setting up a Spring Boot web application from scratch may take some time, this project
is meant to be a good base to work off of, with default configurations and capabilities
working out of the box:

- Authentication (OAuth) :x: 
- Authorization (Method Level Auth) :x:
- CI / CD Pipeline (Gitlab) :white_check_mark:
- End to end on the CI / CD Pipeline :construction_worker:

This is a repository to be used as a base for any Java based web service or API that
is intended to become a fully fledged productive system at any point in time.

## What's inside this repo?
This repository exists so you can go straight to writing business logic and develop
your idea. Let this repository take care of the following:

### Authentication
Encryption at REST enabled by default, with sensible configuration guidelines

### Authorization
Role based access and entity level policy based authorization out of the box

### CI / CD (Gitlab)
CI / CD pre-configured, to build, test and deploy the code to a desired infrastructure

#### Locally Test the CI Job

```bash
sudo gitlab-runner exec docker build-and-test-job --docker-privileged --docker-volumes 'gitlab-runner-exec-docker-cache:/cache'
```

Additionally, local 
### Automation Tests (the good ones)
Automated tests running on the CI: Not the silly unit tests and mocked integration tests, but 
rather the gnarly UI and API fully fledged tests that you always want to do but take so much
time to set up

