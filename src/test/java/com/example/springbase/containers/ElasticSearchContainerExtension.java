package com.example.springbase.containers;

import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.stereotype.Service;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.elasticsearch.ElasticsearchContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
public class ElasticSearchContainerExtension implements BeforeAllCallback, ExtensionContext.Store.CloseableResource {
    private static String ELASTICSEARCH_IMAGE = "docker.elastic.co/elasticsearch/elasticsearch:7.14.2";
    private static String CLUSTER_NAME = "test-cluster";
    public static int ELASTICSEARCH_PORT = 9200;

    @Container
    private static final ElasticsearchContainer elasticsearchContainer = new ElasticsearchContainer(ELASTICSEARCH_IMAGE);

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception {
        elasticsearchContainer.addEnv("cluster.name", CLUSTER_NAME);
        elasticsearchContainer.withExposedPorts(ELASTICSEARCH_PORT)
                        .waitingFor(
                                Wait.forHttp("/_cluster/health")
                                        .forResponsePredicate(s -> s.contains("green") || s.contains("yellow")));
        elasticsearchContainer.start();
    }

    @Override
    public void close() throws Throwable {
        elasticsearchContainer.stop();
    }

    public static ElasticsearchContainer getElasticsearchContainer() {
        return elasticsearchContainer;
    }
}
