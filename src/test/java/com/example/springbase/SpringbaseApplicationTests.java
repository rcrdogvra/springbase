package com.example.springbase;

import com.example.springbase.containers.ElasticSearchContainerExtension;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.elasticsearch.ElasticsearchContainer;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import static com.example.springbase.containers.ElasticSearchContainerExtension.ELASTICSEARCH_PORT;

@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
@ExtendWith(ElasticSearchContainerExtension.class)
class SpringbaseApplicationTests {

	@Test
	void checkHealth() throws URISyntaxException, IOException, InterruptedException {
		ElasticsearchContainer container = ElasticSearchContainerExtension.getElasticsearchContainer();
		HttpClient client = HttpClient.newBuilder().build();
		String url = String.format("http://%s/_cluster/health", container.getHttpHostAddress(), container.getMappedPort(ELASTICSEARCH_PORT));
		HttpRequest healthRequest = HttpRequest.newBuilder()
				.uri(new URI(url))
				.GET()
				.build();
		HttpResponse<String> response = client.send(healthRequest, HttpResponse.BodyHandlers.ofString());
		log.info("[response]: {}", response.body());
	}

}
